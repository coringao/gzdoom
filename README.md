GZDOOM
======

GZDoom is a modder-friendly OpenGL source ZDoom-based port.

ZDoom is a family of enhanced port of the Doom engine to modern operating systems.

**Installation dependency:**
----------------------------

    # apt install cmake g++ libsdl2-dev libbz2-dev libgme-dev libjpeg-dev libgtk-3-dev libfluidsynth-dev

**Compilation:**
----------------

    $ mkdir -p build && cd build

    $ cmake ..

**License:**
-----------

> This is free software: you can redistribute it and/or modify it under
> the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License,
> or any later version.

- (c) Doom Source 1997 id Software, Raven Software and contributors
- (c) Copyright 1998-2018 ZDoom + GZDoom teams and contributors
- (c) Copyright 2018 Individual work by Carlos Donizete Froes [a.k.a coringao]

> Please see license files for individual contributor licenses.
